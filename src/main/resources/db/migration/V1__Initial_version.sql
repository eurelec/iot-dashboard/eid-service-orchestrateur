USE orchestrateur_db;

INSERT INTO device_type (id, name) VALUES
  ('603c01fb-4ebe-47b2-8ce8-896075749495', 'Electrovanne'),
  ('fcd6f86b-9a38-435b-9018-e476c29d8394', 'Pompe'),
  ('58eba76d-5e91-40a2-92b0-53506cea3793', 'Moteur'),
  ('e9a269dd-d33c-4138-8a93-14c284f8365b', 'Lampe')
;