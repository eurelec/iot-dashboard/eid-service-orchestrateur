package com.eurelec.iotdashboard.serviceorchestrateur.mapper;

import com.eurelec.iotdashboard.serviceorchestrateur.dto.*;
import com.eurelec.iotdashboard.serviceorchestrateur.dto.oauth.DeviceRequestDto;
import com.eurelec.iotdashboard.serviceorchestrateur.dto.oauth.DeviceResponseDto;
import com.eurelec.iotdashboard.serviceorchestrateur.entity.Device;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface DeviceMapper {
    DeviceMapper INSTANCE = Mappers.getMapper( DeviceMapper.class );

    Device newDeviceDtoToDevice(NewDeviceDto newUserDto);
    DeviceRequestDto newDeviceDtoToDeviceRequestDto(NewDeviceDto newUserDto);
    Device deviceResponseDtoToDevice(DeviceResponseDto deviceResponseDto);
}
