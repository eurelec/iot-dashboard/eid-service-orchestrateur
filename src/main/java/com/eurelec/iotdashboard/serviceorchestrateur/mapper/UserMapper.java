package com.eurelec.iotdashboard.serviceorchestrateur.mapper;

import com.eurelec.iotdashboard.serviceorchestrateur.dto.NewUserDto;
import com.eurelec.iotdashboard.serviceorchestrateur.dto.oauth.UserRequestDto;
import com.eurelec.iotdashboard.serviceorchestrateur.dto.oauth.UserResponseDto;
import com.eurelec.iotdashboard.serviceorchestrateur.entity.User;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface UserMapper {
    UserMapper INSTANCE = Mappers.getMapper( UserMapper.class );

//    @Mapping(source = "numberOfSeats", target = "seatCount")
    User newUserDtoToUser(NewUserDto newUserDto);
    User oAuthUserResponseDtoToUser(UserResponseDto oAuthUserResponseDto);
    UserRequestDto newUserDtoToOAuthUserRequestDto(NewUserDto newUserDto);
}
