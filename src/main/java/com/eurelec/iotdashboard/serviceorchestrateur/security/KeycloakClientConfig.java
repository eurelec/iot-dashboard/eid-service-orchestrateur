package com.eurelec.iotdashboard.serviceorchestrateur.security;

import feign.RequestInterceptor;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class KeycloakClientConfig {

    private final OAuth2Provider oauth2Provider;
    private final String KEYCLOAK_SERVER_NAME = "keycloak-client";

    @Bean
    public RequestInterceptor KeycloakClientInterceptor() {
        return (requestTemplate) ->{
            requestTemplate.header(
                    HttpHeaders.AUTHORIZATION,
                    oauth2Provider.getAuthenticationToken(KEYCLOAK_SERVER_NAME)
            );
            requestTemplate.header(
                    HttpHeaders.CONTENT_TYPE,
                    MediaType.APPLICATION_JSON_VALUE
            );
        };

    }
}