package com.eurelec.iotdashboard.serviceorchestrateur.api.v1;

import com.eurelec.iotdashboard.serviceorchestrateur.dto.NewUserDto;
import com.eurelec.iotdashboard.serviceorchestrateur.dto.oauth.UserResponseDto;
import com.eurelec.iotdashboard.serviceorchestrateur.entity.User;
import com.eurelec.iotdashboard.serviceorchestrateur.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/users")
public class UserController {

    @Autowired
    UserService userService;

    @GetMapping("/{id}")
    public ResponseEntity<User> getUser(@PathVariable("id") String id) {
        return ResponseEntity.ok(userService.getUserById(id));
    }

    @GetMapping("/secret/{id}")
    public void getUserSecret(@PathVariable("id") String id) {
    }

    @GetMapping
    public ResponseEntity<List<User>> getUsers(){
        return ResponseEntity.ok(userService.getUsers());
    }

    @PostMapping
    public ResponseEntity<UserResponseDto> postCreateUser(@RequestBody NewUserDto newUser){
        return ResponseEntity.ok(userService.createUser(newUser));
    }

    @PutMapping("/{id}")
    public void putUpdateUser(@PathVariable("id") String id){

    }

    @PutMapping("/reset-password/{id}")
    public void putResetPassword(){

    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteUser(@PathVariable("id") String id){
        userService.deleteUser(id);
        return ResponseEntity.noContent().build();
    }

}
