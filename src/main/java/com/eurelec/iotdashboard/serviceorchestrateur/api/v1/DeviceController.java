package com.eurelec.iotdashboard.serviceorchestrateur.api.v1;

import com.eurelec.iotdashboard.serviceorchestrateur.dto.DeviceStatusDto;
import com.eurelec.iotdashboard.serviceorchestrateur.dto.NewDeviceDto;
import com.eurelec.iotdashboard.serviceorchestrateur.entity.Device;
import com.eurelec.iotdashboard.serviceorchestrateur.service.DeviceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/api/v1/devices")
public class DeviceController {

    @Autowired
    DeviceService deviceService;

    @GetMapping("/{id}")
    public ResponseEntity<Device> getDevice(@PathVariable("id") String id) {
        return ResponseEntity.ok(deviceService.getDeviceById(id));
    }

    @GetMapping
    public ResponseEntity<List<Device>> getDevices(){
        return ResponseEntity.ok(deviceService.getAllDevices());
    }

    @PostMapping
    public ResponseEntity<Device> postCreateDevice(@RequestBody NewDeviceDto newDeviceDto){
        return ResponseEntity.ok(deviceService.creatDevice(newDeviceDto));
    }

    @GetMapping("/status/{id}")
    public ResponseEntity<DeviceStatusDto> getStatus(@PathVariable("id") String id){
        return ResponseEntity.ok(deviceService.getStatus(id));
    }

    @PutMapping("/status")
    public ResponseEntity<DeviceStatusDto> putChangeStatus(@RequestBody DeviceStatusDto deviceStatusDto){
        return ResponseEntity.ok(deviceService.changeStatus(deviceStatusDto));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteDevice(@PathVariable("id") String id){
        deviceService.deleteDevice(id);
        return ResponseEntity.noContent().build();
    }

}
