package com.eurelec.iotdashboard.serviceorchestrateur.entity;


import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class User {
    @Id
    private String id;
    @Column(unique = true)
    private String username;
    @Column(unique = true)
    private String email;
    private String firstName;
    private String lastName;
    @JsonIgnore
    @OneToMany(
            targetEntity = Device.class,
            mappedBy = "owner",
            orphanRemoval = true,
            fetch = FetchType.LAZY
    )
    private List<Device> devices;
}
