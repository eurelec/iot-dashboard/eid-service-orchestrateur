package com.eurelec.iotdashboard.serviceorchestrateur.service;

import com.eurelec.iotdashboard.serviceorchestrateur.dto.NewUserDto;
import com.eurelec.iotdashboard.serviceorchestrateur.dto.oauth.UserRequestDto;
import com.eurelec.iotdashboard.serviceorchestrateur.dto.oauth.UserResponseDto;
import com.eurelec.iotdashboard.serviceorchestrateur.entity.User;
import com.eurelec.iotdashboard.serviceorchestrateur.mapper.UserMapper;
import com.eurelec.iotdashboard.serviceorchestrateur.mapper.UserMapperImpl;
import com.eurelec.iotdashboard.serviceorchestrateur.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserService {

    @Autowired
    KeycloakService keycloakService;

    UserMapper userMapper = new UserMapperImpl();

    @Autowired
    UserRepository userRepository;

    public UserResponseDto createUser(NewUserDto newUserDto){
        UserRequestDto authUserRequestDto = userMapper.newUserDtoToOAuthUserRequestDto(newUserDto);
        UserResponseDto authUserResponseDto = keycloakService.createUser(authUserRequestDto);
        User user = userMapper.oAuthUserResponseDtoToUser(authUserResponseDto);
        userRepository.save(user);
        return authUserResponseDto;
    }

    public User getUserById(String id){
        return userRepository.findById(id).orElseThrow(RuntimeException::new);
    }

    public void deleteUser(String id){
        keycloakService.deleteUser(id);
        userRepository.findById(id).ifPresent(user -> userRepository.delete(user));
    }

    public List<User> getUsers() {
        List<User> users = new ArrayList<>();
        for (User user : userRepository.findAll()) {
            users.add(user);
        }
        return users;
    }
}
