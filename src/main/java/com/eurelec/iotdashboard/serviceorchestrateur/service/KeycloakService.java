package com.eurelec.iotdashboard.serviceorchestrateur.service;

import com.eurelec.iotdashboard.serviceorchestrateur.client.KeycloakClient;
import com.eurelec.iotdashboard.serviceorchestrateur.dto.oauth.DeviceRequestDto;
import com.eurelec.iotdashboard.serviceorchestrateur.dto.oauth.DeviceResponseDto;
import com.eurelec.iotdashboard.serviceorchestrateur.dto.oauth.UserRequestDto;
import com.eurelec.iotdashboard.serviceorchestrateur.dto.oauth.UserResponseDto;
import feign.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Service
public class KeycloakService {

    final String URL_SPLIT_CHAR = "/";
    final String HEADER_LOCATION = "location";


    @Autowired
    KeycloakClient keycloakClient;

    UserResponseDto createUser(UserRequestDto userRequestDto){
        try (Response response = keycloakClient.createUser(userRequestDto)){
            if (response.status() != HttpStatus.CREATED.value()) throw new RuntimeException();
            String id = extractIdFormResponse(response);
            return keycloakClient.getUserById(id);
        }catch (Exception e){
            throw new RuntimeException("Error while creating keycloak user");
        }
    }

    public void deleteUser(String id){
        try (Response response = keycloakClient.deleteUserById(id)){
            if (response.status() != HttpStatus.NO_CONTENT.value()) {
                throw new Exception();
            }
        }catch (Exception e){
            throw new RuntimeException("Error while deleting keycloak user");
        }
    }

    private String extractIdFormResponse(Response response){
        if(!response.headers().containsKey(HEADER_LOCATION))
            throw new RuntimeException("Header " + HEADER_LOCATION + " not found!");
        String[] urlParts = response.headers().get(HEADER_LOCATION).toArray()[0].toString().split(URL_SPLIT_CHAR);
        return urlParts[urlParts.length-1];
    }

    public DeviceResponseDto createDeviceClient(DeviceRequestDto oAuthDeviceRequestDto) {
        try (Response response = keycloakClient.createClient(oAuthDeviceRequestDto)){
            if (response.status() != HttpStatus.CREATED.value()) throw new RuntimeException();
            String id = extractIdFormResponse(response);
            return keycloakClient.getClientById(id);
        }catch (Exception e){
            throw new RuntimeException("Error while creating keycloak client");
        }
    }

    public void deleteDeviceClientById(String id) {
        try (Response response = keycloakClient.deleteClientById(id)){
            if (response.status() != HttpStatus.NO_CONTENT.value()) {
                throw new Exception();
            }
        }catch (Exception e){
            throw new RuntimeException("Error while deleting keycloak client");
        }
    }


}
