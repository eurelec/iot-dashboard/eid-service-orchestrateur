package com.eurelec.iotdashboard.serviceorchestrateur.service;

import com.eurelec.iotdashboard.serviceorchestrateur.client.CacheClient;
import com.eurelec.iotdashboard.serviceorchestrateur.dto.DeviceStatusDto;
import com.eurelec.iotdashboard.serviceorchestrateur.dto.NewDeviceDto;
import com.eurelec.iotdashboard.serviceorchestrateur.dto.oauth.DeviceRequestDto;
import com.eurelec.iotdashboard.serviceorchestrateur.dto.oauth.DeviceResponseDto;
import com.eurelec.iotdashboard.serviceorchestrateur.entity.Device;
import com.eurelec.iotdashboard.serviceorchestrateur.entity.DeviceType;
import com.eurelec.iotdashboard.serviceorchestrateur.entity.User;
import com.eurelec.iotdashboard.serviceorchestrateur.mapper.DeviceMapper;
import com.eurelec.iotdashboard.serviceorchestrateur.mapper.DeviceMapperImpl;
import com.eurelec.iotdashboard.serviceorchestrateur.repository.DeviceRepository;
import com.eurelec.iotdashboard.serviceorchestrateur.repository.DeviceTypeRepository;
import com.eurelec.iotdashboard.serviceorchestrateur.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Service
public class DeviceService {

    @Autowired
    KeycloakService keycloakService;

    @Autowired
    DeviceRepository deviceRepository;

    @Autowired
    DeviceTypeRepository deviceTypeRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    CacheClient cacheClient;

    DeviceMapper deviceMapper = new DeviceMapperImpl();

    public Device getDeviceById(String id) {
        return deviceRepository.findById(id).orElseThrow(() -> new RuntimeException("Device not found"));
    }

    public List<Device> getAllDevices() {
        List<Device> devices = new ArrayList<>();
        for (Device device : deviceRepository.findAll()) {
            devices.add(device);
        }
        return devices;
    }

    public Device creatDevice(NewDeviceDto newDeviceDto) {
        DeviceRequestDto requestDto = deviceMapper.newDeviceDtoToDeviceRequestDto(newDeviceDto);
        DeviceResponseDto responseDto = keycloakService.createDeviceClient(requestDto);
        changeStatus(new DeviceStatusDto(responseDto.getId(), false));
        Device device = deviceMapper.deviceResponseDtoToDevice(responseDto);
        device.setDescription(newDeviceDto.getDescription());
        DeviceType type = deviceTypeRepository.findById(newDeviceDto.getTypeId()).orElse(new DeviceType());
        User user = userRepository.findById(newDeviceDto.getOwnerId()).orElse(new User());
        device.setOwner(user);
        device.setType(type);
        return deviceRepository.save(device);
    }

    public void deleteDevice(String id){
        keycloakService.deleteDeviceClientById(id);
        deviceRepository.findById(id).ifPresent(user -> deviceRepository.delete(user));
    }

    public DeviceStatusDto getStatus(String id) {
        return cacheClient.getStatus(id);
    }

    public DeviceStatusDto changeStatus(DeviceStatusDto deviceStatusDto) {
        return cacheClient.putStatus(deviceStatusDto);
    }
}
