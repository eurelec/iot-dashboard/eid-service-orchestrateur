package com.eurelec.iotdashboard.serviceorchestrateur.client;

import com.eurelec.iotdashboard.serviceorchestrateur.dto.oauth.DeviceResponseDto;
import com.eurelec.iotdashboard.serviceorchestrateur.dto.oauth.UserRequestDto;
import com.eurelec.iotdashboard.serviceorchestrateur.dto.oauth.UserResponseDto;
import com.eurelec.iotdashboard.serviceorchestrateur.security.KeycloakClientConfig;
import feign.Response;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

@FeignClient(
        name = "KeycloakClient",
        url = "http://localhost:9090/admin/realms/master",
        configuration = KeycloakClientConfig.class
)
public interface KeycloakClient {

    @GetMapping(value = "/clients/{id}")
    DeviceResponseDto getClientById(@PathVariable("id") String id);

    @PostMapping(value = "/clients")
    Response createClient(@RequestBody Object body);

    @DeleteMapping(value = "/clients/{id}")
    Response deleteClientById(@PathVariable("id") String id);

    @PostMapping(value = "/users")
    Response createUser(@RequestBody UserRequestDto userRequestDto);

    @GetMapping(value = "/users/{id}")
    UserResponseDto getUserById(@PathVariable("id") String id);

    @DeleteMapping(value = "/users/{id}")
    Response deleteUserById(@PathVariable("id") String id);

}
