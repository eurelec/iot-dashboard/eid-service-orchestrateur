package com.eurelec.iotdashboard.serviceorchestrateur.client;

import com.eurelec.iotdashboard.serviceorchestrateur.dto.DeviceStatusDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(
        name = "CacheClient",
        url = "http://localhost:8081/api/v1/status"
)
public interface CacheClient {
    @GetMapping(value = "/{id}")
    DeviceStatusDto getStatus(@PathVariable("id") String id);
    @PutMapping()
    DeviceStatusDto putStatus(@RequestBody DeviceStatusDto status);
}
