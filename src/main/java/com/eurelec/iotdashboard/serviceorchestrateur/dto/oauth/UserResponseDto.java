package com.eurelec.iotdashboard.serviceorchestrateur.dto.oauth;

import com.eurelec.iotdashboard.serviceorchestrateur.dto.BaseUserDto;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class UserResponseDto extends BaseUserDto {
    private Boolean emailVerified;
    private Boolean enabled;
}
