package com.eurelec.iotdashboard.serviceorchestrateur.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class BaseUserDto {
    private String id = null;
    private String firstName;
    private String lastName;
    private String username;
    private String email;
}
