package com.eurelec.iotdashboard.serviceorchestrateur.dto.oauth;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class DeviceResponseDto {
    private String id;
    private String name;
    private String secret;
}
