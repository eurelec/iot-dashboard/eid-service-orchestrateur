package com.eurelec.iotdashboard.serviceorchestrateur.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class NewUserDto extends BaseUserDto {
    private String password;
}
