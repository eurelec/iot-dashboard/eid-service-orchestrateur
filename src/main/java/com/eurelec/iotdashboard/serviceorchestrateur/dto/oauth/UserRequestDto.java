package com.eurelec.iotdashboard.serviceorchestrateur.dto.oauth;

import com.eurelec.iotdashboard.serviceorchestrateur.dto.BaseUserDto;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class UserRequestDto extends BaseUserDto {
    private Boolean emailVerified = true;
    private Boolean enabled = true;
    private List<CredentialDto> credentials;
}
