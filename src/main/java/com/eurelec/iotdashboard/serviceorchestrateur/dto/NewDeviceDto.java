package com.eurelec.iotdashboard.serviceorchestrateur.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class NewDeviceDto {
    private String name;
    private String description;
    private String ownerId;
    private String typeId;
}
