package com.eurelec.iotdashboard.serviceorchestrateur.repository;

import com.eurelec.iotdashboard.serviceorchestrateur.entity.Device;
import com.eurelec.iotdashboard.serviceorchestrateur.entity.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DeviceRepository extends CrudRepository<Device, String> {
}
