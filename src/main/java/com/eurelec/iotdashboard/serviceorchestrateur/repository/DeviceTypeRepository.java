package com.eurelec.iotdashboard.serviceorchestrateur.repository;

import com.eurelec.iotdashboard.serviceorchestrateur.entity.DeviceType;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DeviceTypeRepository extends CrudRepository<DeviceType, String> {
}
