package com.eurelec.iotdashboard.serviceorchestrateur;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class ServiceOrchestrateurApplication {

    public static void main(String[] args) {
        SpringApplication.run(ServiceOrchestrateurApplication.class, args);
 }

}
